"use strict";

let notify = require('gulp-notify');

module.exports = options => {
    return notify(options);
};

module.exports.withJpMessage = options => {
    let _message = options.message ? '\n' + options.message : '';
    options.message = getTime() + _message;
    return notify(options);
};

function getTime() {
    let date = new Date();
    let Y = date.getFullYear();
    let M = date.getMonth();
    let D = date.getDate();
    let H = date.getHours();
    let m = date.getMinutes();
    let s = date.getSeconds();
    return timeComment(date) + ' ' + Y + '/' + M + '/' + D + ' ' + H + ':' + m + ':' + s;
}

function timeComment(date) {
    if (date instanceof Date) {
        let hour = date.getHours().valueOf();
        if (0 <= hour && hour < 3) {
            return randComment([
                'てっぺん超えてるよ？(*_*;)',
                'あんまり無理するなよ(-_-)zzz',
                'レッドブルでも飲んでやるか!?( ^o^)ﾉ',
                '睡眠不足は生産効率の低下(-.-)'
            ]);
        } else if (3 <= hour && hour < 6) {
            return randComment([
                '1番眠たい時間(*_*;)',
                'これは早起き？夜更かし？(-_-)zzz',
                'なちゅらるは〜〜〜い( ^o^)ﾉ',
                'ああ、やってられん(-.-)'
            ]);
        } else if (6 <= hour && hour < 9) {
            return randComment([
                'おっはよ〜〜〜！(^o^)',
                'もう少し寝ていたなあ(-_-)zzz',
                '今日もよろしく！！( ^o^)ﾉ',
                'さて、今日のタスクは... (-.-)'
            ]);
        } else if (9 <= hour && hour < 11) {
            return randComment([
                'そろそろ120がOPENする時間(•́⌄•́๑)૭✧',
                '朝ご飯食べた？?(ο´･д･)??',
                'すがすがしい朝だね!?( ^o^)ﾉ'
            ]);
        } else if (11 <= hour && hour < 12) {
            return randComment([
                '調子でてきたんじゃない?(^o^)',
                '今日の天気はいかがですか?(･ω･ )',
                'そろそろお昼だなあ...(･ω･｀)',
                'ちょ！昼飯どうする??(°Д°≡°Д°)?'
            ]);
        } else if (12 <= hour && hour < 14) {
            return randComment([
                'お昼ご飯はもう食べた？J( ’ー`)し',
                '昼飯食ったら眠くなる頃(-_-)zzz',
                '眠気に負けるな！！頑張れ〜( ^o^)ﾉ',
                '眠いときはいっそ寝ればいい(-.-)'
            ]);
        } else if (14 <= hour && hour < 15) {
            return randComment([
                'さあ、昼からもいっちょやっか?(^o^)',
                'ああ、そろそろおやつ...(･ω･ )',
                'ちょっと息抜きしたくない?(･ω･｀)',
                '本日のスイーツは??(°Д°≡°Д°)?'
            ]);
        } else if (15 <= hour && hour < 17) {
            return randComment([
                'さあ、もう一踏ん張り(•́⌄•́๑)૭✧',
                '姿勢だいじょぶ?(･ω･ )'
            ]);
        } else if (17 <= hour && hour < 18) {
            return randComment([
                'さあ、そろそろ今日のお仕事目処付けよう(^o^)',
                '今日の進捗どない?(･ω･ )',
                'さあ、そろそろお腹空くね(･ω･｀)',
                '晩飯どーしよ??(°Д°≡°Д°)?'
            ]);
        } else if (18 <= hour && hour < 20) {
            return randComment([
                '実はこっからが1番ノル時間?(^o^)',
                '息抜き大事(･ω･ )',
                '家庭はだいじょぶ?(･ω･｀)',
                '仕事しすぎじゃね??(°Д°≡°Д°)?'
            ]);
        } else if (20 <= hour && hour < 21) {
            return randComment([
                '本日もお疲れ様です(^o^)',
                'そろそろ120閉店ガラガラ(･ω･ )',
                '晩ご飯食べた?(･ω･｀)',
                'テンション上がってきたーーー!\nｷﾀｷﾀｷﾀ━━━(ﾟ∀ﾟ≡(ﾟ∀ﾟ≡ﾟ∀ﾟ)≡ﾟ∀ﾟ)━━━━!!!!!!!!!\n'
            ]);
        } else if (21 <= hour && hour < 23) {
            return randComment([
                '夜だ───-( ﾟAﾟ )─────!!',
                '━━(Α｀(○=(く_`(○=(εﾟ(○=(ﾟ∀ﾟ)=○)Дﾟ)=○)ω･`)=○)ﾟё)=○)ﾟдゝ)━━!!!\n',
                '☆いぇ━━━━い★ヽ(*ﾟДﾟ)人(ﾟДﾟ*)ﾉ★いぇ━━━━━い☆\n'
            ]);
        } else if (23 <= hour && hour < 24) {
            return randComment([
                '(・∀・。)なんぢゃなんぢゃ(。・∀・)\n',
                '(´・ω・｀)モキュ？',
                'そろそろ寝ない?(；´∀｀)ゞﾝｰﾄｫ…'
            ]);
        }
    }

    function randComment(comments) {
        if (comments instanceof Array) {
            let r = Math.floor(Math.random()*comments.length);
            return comments[r];
        }
        return comments;
    }
}