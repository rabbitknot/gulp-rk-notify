let through = require('through2');

"use strict";

let disable = !!process.env.DISABLE_NOTIFIER;

let noopStream = function () { return through.obj(); };

module.exports = disable ? noopStream : require('./lib/notify');
