/*global describe, it*/
"use strict";

let should = require('should');
let notify = require('../');

let mockGenerator = tester => {
    tester = tester || function () {  };
    return (opts, callback) => {
        tester(opts);
        callback();
    };
};

describe('gulp output stream', () => {

    describe('notify()', () => {
        it('should return a stream', done => {
            let stream = notify({
                notifier: mockGenerator()
            });
        should.exist(stream);
        should.exist(stream.on);
        should.exist(stream.pipe);
        done();
        });
    });
});
